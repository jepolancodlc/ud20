package Dflt.views;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class view04 extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;


	public view04() {
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Eventos");
		lblNewLabel.setBounds(23, 94, 56, 16);
		contentPane.add(lblNewLabel);
		
		final JTextArea txtTextoInicial = new JTextArea();
		txtTextoInicial.setText("Texto Inicial");
		txtTextoInicial.setBounds(82, 13, 338, 213);
		contentPane.add(txtTextoInicial);
		
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowActivated(WindowEvent e) { txtTextoInicial.append("\nWindows Activated");
			}
			@Override
			public void windowClosed(WindowEvent e) {txtTextoInicial.append("\nWindows Closed");
			}
			@Override
			public void windowDeiconified(WindowEvent e) {txtTextoInicial.append("\nWindows Deiconified");
			
			}
			@Override
			public void windowIconified(WindowEvent e) {txtTextoInicial.append("\nWindows Iconified");
			
			}
		});
	}
}

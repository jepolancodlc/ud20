package Dflt.App;

import java.awt.EventQueue;

import Dflt.views.view06;

public class App {
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					view06 frame = new view06();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}


}

package Dflt.views;

import java.awt.EventQueue;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.*;

public class view02 extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	
	public view02() {
		setTitle("Ejercicio 2");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lbl1 = new JLabel("Has pulsado:");
		lbl1.setBounds(36, 62, 107, 16);
		contentPane.add(lbl1);

		final JLabel lbl2 = new JLabel("");
		lbl2.setBounds(115, 62, 56, 16);
		contentPane.add(lbl2);
		
		JButton btnNewButton = new JButton("Boton 1");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) { lbl2.setText("Boton 1");
			}
		});
		btnNewButton.setBounds(195, 58, 97, 25);
		contentPane.add(btnNewButton);
		
		JButton btnBoton = new JButton("Boton 2");
		btnBoton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) { lbl2.setText("Boton 2");
			}
		});
		btnBoton.setBounds(304, 58, 97, 25);
		contentPane.add(btnBoton);
		
	}
}

package Dflt.views;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JCheckBox;
import javax.swing.ButtonGroup;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class view07 extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldConvertir;
	private JTextField textFieldrRes;
	double resultado;
	private JButton btnNewButton_1;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	public view07() {
		setTitle("Convertidor Moneda");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 158);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Cantidad a convertir");
		lblNewLabel.setBounds(12, 26, 115, 16);
		contentPane.add(lblNewLabel);
		
		JLabel lblPesokg = new JLabel("Resultado");
		lblPesokg.setBounds(252, 26, 56, 16);
		contentPane.add(lblPesokg);

		final JCheckBox chckbxNewCheckBox = new JCheckBox("Ptas a Euros");
		buttonGroup.add(chckbxNewCheckBox);
		chckbxNewCheckBox.setBounds(12, 65, 113, 25);
		contentPane.add(chckbxNewCheckBox);
		
		final JCheckBox chckbxEurosAPtas = new JCheckBox("Euros a Ptas");
		buttonGroup.add(chckbxEurosAPtas);
		chckbxEurosAPtas.setBounds(138, 65, 113, 25);
		contentPane.add(chckbxEurosAPtas);

		
		textFieldConvertir = new JTextField();
		textFieldConvertir.setBounds(134, 23, 86, 22);
		contentPane.add(textFieldConvertir);
		textFieldConvertir.setColumns(10);
		
		textFieldrRes = new JTextField();
		textFieldrRes.setEditable(false);
		textFieldrRes.setColumns(10);
		textFieldrRes.setBounds(318, 23, 86, 22);
		contentPane.add(textFieldrRes);
		
		btnNewButton_1 = new JButton("Cambiar");
		btnNewButton_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) { 
				double aConvertir= Double.parseDouble(textFieldConvertir.getText());
				
				if (chckbxEurosAPtas.isSelected()) { resultado = (aConvertir * 166.368);	
				}	else if (chckbxNewCheckBox.isSelected()) { resultado = (aConvertir/166.368);	
				}

				textFieldrRes.setText(Double.toString(Math.round(resultado * 100) / 100d));
				
			}
		});
		btnNewButton_1.setBounds(318, 65, 97, 25);
		contentPane.add(btnNewButton_1);
			}
}

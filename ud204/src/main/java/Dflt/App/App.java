package Dflt.App;

import java.awt.EventQueue;

import Dflt.views.view04;

public class App {

	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					view04 frame = new view04();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}

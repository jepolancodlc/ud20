package Dflt.views;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class view05 extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	
	public view05() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		
		final JTextArea txtrTextoInicia = new JTextArea();
		txtrTextoInicia.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) { txtrTextoInicia.append("\n Mouse Clicked");
			}
			@Override
			public void mouseEntered(MouseEvent e) { txtrTextoInicia.append("\n Mouse Entered");
			}
			@Override
			public void mousePressed(MouseEvent e) { txtrTextoInicia.append("\n Mouse Pressed");
			}
			@Override
			public void mouseExited(MouseEvent e) {txtrTextoInicia.append("\n Mouse Exited");
			}
			@Override
			public void mouseReleased(MouseEvent e) {txtrTextoInicia.append("\n Mouse Released");
			}
		});
		txtrTextoInicia.setText("Texto Inicial");
		txtrTextoInicia.setBounds(12, 69, 408, 171);
		contentPane.add(txtrTextoInicia);
		
		JButton btnNewButton = new JButton("Limpiar");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) { txtrTextoInicia.setText("");
			}
		});
		btnNewButton.setBounds(162, 31, 97, 25);
		contentPane.add(btnNewButton);

		
	}

}

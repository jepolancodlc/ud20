package Dflt.views;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.*;

public class view06 extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textFieldAltura;
	private JTextField textFieldPeso;
	private JTextField textField;
	private JButton btnCalcularImc;
	double resultado;
	public view06() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 150);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Altura(Metros):");
		lblNewLabel.setBounds(12, 26, 88, 16);
		contentPane.add(lblNewLabel);
		
		JLabel lblPesokg = new JLabel("Peso(Kg):");
		lblPesokg.setBounds(205, 26, 56, 16);
		contentPane.add(lblPesokg);
		
		textFieldAltura = new JTextField();
		textFieldAltura.setBounds(107, 23, 86, 22);
		contentPane.add(textFieldAltura);
		textFieldAltura.setColumns(10);
		
		textFieldPeso = new JTextField();
		textFieldPeso.setColumns(10);
		textFieldPeso.setBounds(273, 23, 86, 22);
		contentPane.add(textFieldPeso);
		
		textField = new JTextField();
		textField.setEditable(false);
		textField.setColumns(10);
		textField.setBounds(228, 62, 86, 22);
		contentPane.add(textField);
		
		JLabel lblImc = new JLabel("IMC");
		lblImc.setBounds(194, 65, 22, 16);
		contentPane.add(lblImc);
		
	
		btnCalcularImc = new JButton("Calcular IMC");
		btnCalcularImc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) { 				
				double altura=Double.parseDouble(textFieldAltura.getText()); 	
				double peso=Double.parseDouble(textFieldPeso.getText()); 
				resultado= peso / (altura*altura);
				textField.setText(Double.toString(Math.round(resultado * 100) / 100d));
			}
		});
		btnCalcularImc.setBounds(64, 60, 122, 25);
		contentPane.add(btnCalcularImc);
	}

}

package Dflt.views;


import javax.swing.border.EmptyBorder;
import java.awt.event.*;
import javax.swing.*;

public class view08 extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textFieldConvertir;
	private JTextField textFieldrRes;
	double resultado;
	private JButton btnNewButton_1;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	public view08() {
		setTitle("Convertidor Moneda");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 469, 158);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Cantidad a convertir");
		lblNewLabel.setBounds(12, 26, 115, 16);
		contentPane.add(lblNewLabel);
		
		JLabel lblPesokg = new JLabel("Resultado");
		lblPesokg.setBounds(252, 26, 56, 16);
		contentPane.add(lblPesokg);

		final JCheckBox chckbxNewCheckBox = new JCheckBox("Ptas a Euros");
		buttonGroup.add(chckbxNewCheckBox);
		chckbxNewCheckBox.setBounds(12, 65, 113, 25);
		contentPane.add(chckbxNewCheckBox);
		
		final JCheckBox chckbxEurosAPtas = new JCheckBox("Euros a Ptas");
		buttonGroup.add(chckbxEurosAPtas);
		chckbxEurosAPtas.setBounds(129, 65, 113, 25);
		contentPane.add(chckbxEurosAPtas);

		
		textFieldConvertir = new JTextField();
		textFieldConvertir.setBounds(134, 23, 86, 22);
		contentPane.add(textFieldConvertir);
		textFieldConvertir.setColumns(10);
		
		textFieldrRes = new JTextField();
		textFieldrRes.setEditable(false);
		textFieldrRes.setColumns(10);
		textFieldrRes.setBounds(318, 23, 86, 22);
		contentPane.add(textFieldrRes);
		
		btnNewButton_1 = new JButton("Cambiar");
		btnNewButton_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) { 
				double aConvertir= Double.parseDouble(textFieldConvertir.getText());
				
				if (chckbxEurosAPtas.isSelected()) { resultado = (aConvertir * 166.368);	
				}	else if (chckbxNewCheckBox.isSelected()) { resultado = (aConvertir/166.368);	
				}

				textFieldrRes.setText(Double.toString(Math.round(resultado * 100) / 100d));
				
			}
		});
		btnNewButton_1.setBounds(339, 65, 100, 25);
		contentPane.add(btnNewButton_1);
		
		JButton btnBorrar = new JButton("Borrar");
		btnBorrar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) { 	
			textFieldrRes.setText(""); 	
			textFieldConvertir.setText("");
			}
		});
		btnBorrar.setBounds(246, 65, 81, 25);
		contentPane.add(btnBorrar);
			}
}

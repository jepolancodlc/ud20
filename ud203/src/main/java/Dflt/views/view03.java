package Dflt.views;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.*;

public class view03 extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	int contadorB1 = 0;
	int contadorB2 = 0;

	public view03() {
		setTitle("Ejercicio 3");
		

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 398, 190);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Clicks Boton 1:");
		lblNewLabel.setBounds(28, 51, 89, 16);
		contentPane.add(lblNewLabel);
		
		JLabel lblClicksBoton = new JLabel("Clicks Boton 2:");
		lblClicksBoton.setBounds(225, 51, 89, 16);
		contentPane.add(lblClicksBoton);
		

		final JLabel lblCont1 = new JLabel("");
		lblCont1.setBounds(117, 51, 56, 16);
		contentPane.add(lblCont1);
	
		final JLabel lblCont2 = new JLabel("");
		lblCont2.setBounds(319, 51, 56, 16);
		contentPane.add(lblCont2);
		

		JButton btnNewButton = new JButton("Boton 1");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				contadorB1++;
				lblCont1.setText(String.valueOf(contadorB1));
			}
		});
		
		
		btnNewButton.setBounds(28, 80, 97, 25);
		contentPane.add(btnNewButton);
		
		JButton btnBoton = new JButton("Boton 2");
		btnBoton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				contadorB2++;
				lblCont2.setText(String.valueOf(contadorB2));
			}
		});
		btnBoton.setBounds(225, 80, 97, 25);
		contentPane.add(btnBoton);
	}

}

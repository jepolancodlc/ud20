package Dflt.views;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.*;

public class view09 extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	protected JToggleButton tbRojo2,tbAzul2,tbNaranja2,tbBlanco1,tbBlanco2,tbMagenta2, tbAmarillo2
	,tbNegro2,tbRojo1,tbVerde1,tbNegro1,tbAmarillo1,tbAzul1,tbNaranja1,tbMagenta1,tbVerde2;	
	
	
	public view09() {
		setTitle("Juego de formar parejas");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 555, 547);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		//Botones De colores
		//////ROJO
		tbRojo1 = new JToggleButton("");
		tbRojo1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if ((!tbRojo1.isSelected()) && (!tbRojo2.isSelected())) { 
					tbRojo2.setVisible(false); 
					tbRojo1.setVisible(false); 
				}
			}
		});
		tbRojo1.setSelected(true);
		tbRojo1.setBackground(new Color(178, 34, 34));
		tbRojo1.setBounds(141, 255, 116, 108);
		contentPane.add(tbRojo1);
		
		tbRojo2 = new JToggleButton("");
		tbRojo2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if ((!tbRojo2.isSelected()) && (!tbRojo1.isSelected())) { 
					tbRojo2.setVisible(false); 
					tbRojo1.setVisible(false); 
				}
			}
		});
		tbRojo2.setSelected(true);
		tbRojo2.setBackground(new Color(178, 34, 34));
		tbRojo2.setBounds(13, 13, 116, 108);
		contentPane.add(tbRojo2); 
	
		////////AZUL
		tbAzul1 = new JToggleButton("");
		tbAzul1.setBackground(new Color(0, 0, 205));
		tbAzul1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) { 
				if ((!tbAzul1.isSelected()) && (!tbAzul2.isSelected())) { 
					tbAzul1.setVisible(false); 
					tbAzul2.setVisible(false); 
				}
			}
		});
		tbAzul1.setSelected(true);
		tbAzul1.setBounds(398, 13, 116, 108);
		contentPane.add(tbAzul1);
		
		tbAzul2 = new JToggleButton("");
		tbAzul2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if ((!tbAzul2.isSelected()) && (!tbAzul1.isSelected())) { 
					tbAzul1.setVisible(false); 
					tbAzul2.setVisible(false); 
				}
			}
		});
		tbAzul2.setSelected(true);
		tbAzul2.setBackground(new Color(0, 0, 205));
		tbAzul2.setBounds(141, 13, 116, 108);
		contentPane.add(tbAzul2);
		
		///////NARANJA
		tbNaranja1 = new JToggleButton("");
		tbNaranja1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if ((!tbNaranja1.isSelected()) && (!tbNaranja2.isSelected())) { 
					tbNaranja2.setVisible(false); 
					tbNaranja1.setVisible(false); 	
				}
			}
		});
		tbNaranja1.setSelected(true);
		tbNaranja1.setBackground(Color.ORANGE);
		tbNaranja1.setBounds(398, 381, 116, 108);
		contentPane.add(tbNaranja1);

		tbNaranja2 = new JToggleButton("");
		tbNaranja2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if ((!tbNaranja2.isSelected()) && (!tbNaranja1.isSelected())) { 
					tbNaranja2.setVisible(false); 
					tbNaranja1.setVisible(false); 
				}
			}
		});
		tbNaranja2.setSelected(true);
		tbNaranja2.setBackground(Color.ORANGE);
		tbNaranja2.setBounds(269, 13, 116, 108);
		contentPane.add(tbNaranja2);
		
		//////AMARILLO
		 tbAmarillo1 = new JToggleButton("");
		 tbAmarillo1.addMouseListener(new MouseAdapter() {
		 	@Override
		 	public void mouseClicked(MouseEvent e) {
		 		if ((!tbAmarillo1.isSelected()) && (!tbAmarillo2.isSelected())) { 
				
		 			tbAmarillo2.setVisible(false); 
		 			tbAmarillo1.setVisible(false);
				}
		 	}
		 });
		tbAmarillo1.setSelected(true);
		tbAmarillo1.setBackground(Color.YELLOW);
		tbAmarillo1.setBounds(269, 381, 116, 108);
		contentPane.add(tbAmarillo1);		
		
		tbAmarillo2 = new JToggleButton("");
		tbAmarillo2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if ((!tbAmarillo2.isSelected()) && (!tbAmarillo1.isSelected())) { 
					tbAmarillo2.setVisible(false); 
					tbAmarillo1.setVisible(false); 
				}
			}
		});
		tbAmarillo2.setSelected(true);
		tbAmarillo2.setBackground(Color.YELLOW);
		tbAmarillo2.setBounds(269, 134, 116, 108);
		contentPane.add(tbAmarillo2);
		
		///NEGROS
		tbNegro1 = new JToggleButton("");
		tbNegro1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if ((!tbNegro1.isSelected()) && (!tbNegro2.isSelected())) { 
					
					tbNegro1.setVisible(false); 
					tbNegro2.setVisible(false);
				}
			}
		});
		tbNegro1.setSelected(true);
		tbNegro1.setBackground(Color.BLACK);
		tbNegro1.setBounds(12, 381, 116, 108);
		contentPane.add(tbNegro1);
		
		tbNegro2 = new JToggleButton("");
		tbNegro2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if ((!tbNegro2.isSelected()) && (!tbNegro1.isSelected())) { 
					
					tbNegro1.setVisible(false); 
					tbNegro2.setVisible(false);
				}
			}
		});
		tbNegro2.setSelected(true);
		tbNegro2.setBackground(Color.BLACK);
		tbNegro2.setBounds(12, 255, 116, 108);
		contentPane.add(tbNegro2);
		
		//VERDE
		tbVerde1 = new JToggleButton("");
		tbVerde1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if ((!tbVerde1.isSelected()) && (!tbVerde2.isSelected())) { 
					
					tbVerde2.setVisible(false); 
					tbVerde1.setVisible(false);
				}
			}
		});
		tbVerde1.setSelected(true);
		tbVerde1.setBackground(Color.GREEN);
		tbVerde1.setBounds(269, 255, 116, 108);
		contentPane.add(tbVerde1);

		tbVerde2 = new JToggleButton("");
		tbVerde2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if ((!tbVerde2.isSelected()) && (!tbVerde1.isSelected())) { 
					
					tbVerde1.setVisible(false); 
					tbVerde2.setVisible(false); 
				}
			}
		});
		tbVerde2.setSelected(true);
		tbVerde2.setBackground(Color.GREEN);
		tbVerde2.setBounds(398, 134, 116, 108);
		contentPane.add(tbVerde2);
		
		//BLANCO
		tbBlanco1 = new JToggleButton("");
		tbBlanco1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if ((!tbBlanco1.isSelected()) && (!tbBlanco2.isSelected())) { 
					
					tbBlanco2.setVisible(false);
					tbBlanco1.setVisible(false);
				}
			}
		});
		tbBlanco1.setSelected(true);
		tbBlanco1.setBackground(Color.WHITE);
		tbBlanco1.setBounds(141, 381, 116, 108);
		contentPane.add(tbBlanco1);
		
		tbBlanco2  = new JToggleButton("");
		tbBlanco2.setSelected(true);
		tbBlanco2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if ((!tbBlanco2.isSelected()) && (!tbBlanco1.isSelected())) { 

					tbBlanco2.setVisible(false); 
					tbBlanco1.setVisible(false);
				}
			}
		});
		tbBlanco2.setBackground(Color.WHITE);
		tbBlanco2.setBounds(12, 134, 116, 108);
		contentPane.add(tbBlanco2);
		
		///MAGENTA
		tbMagenta1 = new JToggleButton("");
		tbMagenta1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if ((!tbMagenta1.isSelected()) && (!tbMagenta2.isSelected())) { 					
					tbMagenta1.setVisible(false); 
					tbMagenta2.setVisible(false); 	
				}
			}
		});
		tbMagenta1.setSelected(true);
		tbMagenta1.setBackground(Color.MAGENTA);
		tbMagenta1.setBounds(398, 255, 116, 108);
		contentPane.add(tbMagenta1);
		
		tbMagenta2 = new JToggleButton("");
		tbMagenta2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if ((!tbMagenta2.isSelected()) && (!tbMagenta1.isSelected())) { 
					
					tbMagenta2.setVisible(false);
					tbMagenta1.setVisible(false);
				}
			}
		});
		tbMagenta2.setSelected(true);
		tbMagenta2.setBackground(Color.MAGENTA);
		tbMagenta2.setBounds(141, 134, 116, 108);
		contentPane.add(tbMagenta2);
	}
	
}
	 

	

